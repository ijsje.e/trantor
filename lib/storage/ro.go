package storage

import (
	"errors"
	"io"
)

type roStore struct {
	store Store
}

func (st *roStore) Create(id string, name string) (io.WriteCloser, error) {
	return nil, errors.New("Can't create, RO storage")
}

func (st *roStore) Store(id string, file io.Reader, name string) (size int64, err error) {
	return 0, errors.New("Can't store, RO storage")
}

func (st *roStore) Get(id string, name string) (File, error) {
	return st.store.Get(id, name)
}

func (st *roStore) Delete(id string) error {
	return errors.New("Can't delete, RO storage")
}
