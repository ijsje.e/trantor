package trantor

import (
	log "github.com/cihub/seelog"

	"time"

	"gitlab.com/trantor/trantor/lib/database"
)

const (
	minutesUpdateLogger = 5
)

func InitTasks(db database.DB, loggerConfig string) {
	updateLogger := func() error {
		return UpdateLogger(loggerConfig)
	}
	go tasker(updateLogger, minutesUpdateLogger)
}

func tasker(task func() error, minutes int) {
	periodicity := time.Duration(minutes) * time.Minute

	for true {
		time.Sleep(periodicity)
		err := task()
		if err != nil {
			log.Error("Task error: ", err)
		}
	}
}
